//
//  Club_BiApp.swift
//  Club_Bi
//
//  Created by Carlos Méndez on 05/07/21.
//

import SwiftUI
import Firebase

@main
struct Club_BiApp: App {
    
    let persistenceController = PersistenceController.shared

    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
